package org.example;

import org.example.model.Bonus;
import org.junit.jupiter.api.Test;
import java.math.BigDecimal;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class bonusSchemeTest {
    bonusScheme bonus = new bonusScheme();
@Test
    public void CheckBonus_DepositUnder5_Returns0() {
       Bonus testValue = bonus.checkBonus(3.0);
       assertEquals(testValue.getAmount(), new BigDecimal(0));
    }
    @Test
    public void CheckBonus_DepositBetween5And10_Returns5() {
        Bonus testValue = bonus.checkBonus(3.0);
        assertEquals(testValue.getAmount(), new BigDecimal(0));
    }
    @Test
    public void CheckBonus_DepositUnder5_Returns0() {
        Bonus testValue = bonus.checkBonus(3.0);
        assertEquals(testValue.getAmount(), new BigDecimal(0));
    }

}
