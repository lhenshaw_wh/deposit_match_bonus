package org.example;
import org.example.model.Bonus;

import java.math.BigDecimal;
import java.util.Random;


public class bonusScheme {
     double deposit;
     Bonus bonus;
     boolean bonusHit;

    public Bonus checkBonus(double deposit) {
        bonus = new Bonus();
        if (deposit < 5) {
            bonus.setAmount(new BigDecimal(0));
        } else if (deposit >= 5 && deposit <= 10) {
            bonusHit = true;
            bonus.setAmount(new BigDecimal(5));
        } else {
            int a = new Random().nextInt(100);
            if (a < 25) {
                bonusHit = true;
                bonus.setAmount(new BigDecimal(deposit / 2));;
            }
        }
        return bonus;
    }
}